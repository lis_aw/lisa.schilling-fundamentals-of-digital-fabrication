EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12447 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4100 4600 3900 4600
Text Label 3900 4600 2    70   ~ 0
VCC
Wire Wire Line
	4100 4700 3900 4700
Text Label 3900 4700 2    70   ~ 0
VCC
Wire Wire Line
	4100 4900 3900 4900
Text Label 3900 4900 2    70   ~ 0
VCC
Wire Wire Line
	2800 3500 2500 3500
Text Label 2500 3500 2    70   ~ 0
VCC
Wire Wire Line
	2350 7200 2650 7200
Wire Wire Line
	2650 7200 3150 7200
Wire Wire Line
	3650 7200 3150 7200
Wire Wire Line
	3150 6900 3150 7200
Wire Wire Line
	2650 6900 2650 7200
Wire Wire Line
	3650 6900 3650 7200
Connection ~ 3150 7200
Connection ~ 2650 7200
Text Label 2350 7200 0    10   ~ 0
VCC
Wire Wire Line
	4100 4100 3300 4100
Wire Wire Line
	3300 4100 2600 4100
Wire Wire Line
	2600 4100 2600 3800
Connection ~ 3300 4100
Wire Wire Line
	4100 4300 3300 4300
Wire Wire Line
	3300 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4600
Connection ~ 3300 4300
Wire Wire Line
	2350 6300 2650 6300
Wire Wire Line
	2650 6300 3150 6300
Wire Wire Line
	3150 6300 3650 6300
Wire Wire Line
	2650 6600 2650 6300
Wire Wire Line
	3150 6600 3150 6300
Wire Wire Line
	3650 6600 3650 6300
Connection ~ 2650 6300
Connection ~ 3150 6300
Text Label 2350 6300 0    10   ~ 0
GND
Wire Wire Line
	4100 5700 3900 5700
Text Label 3900 5700 2    70   ~ 0
GND
Wire Wire Line
	4100 5800 3900 5800
Text Label 3900 5800 2    70   ~ 0
GND
Wire Wire Line
	2300 3800 2100 3800
Text Label 2100 3800 2    70   ~ 0
GND
Wire Wire Line
	2300 4600 2100 4600
Text Label 2100 4600 2    70   ~ 0
GND
Wire Wire Line
	4100 5500 3900 5500
Text Label 3900 5500 2    70   ~ 0
GND
Wire Wire Line
	8600 5800 8700 5800
Text Label 8700 5800 0    70   ~ 0
GND
Wire Wire Line
	8100 5800 8200 5800
Wire Wire Line
	3200 3500 3200 2900
Wire Wire Line
	3200 2900 3500 2900
Wire Wire Line
	4100 3500 3200 3500
Connection ~ 3200 3500
$Comp
L satshakit_cnc-eagle-import:ATMEGA48_88_168-AU MICRO1
U 1 1 B7F66DC5
P 5300 4600
F 0 "MICRO1" H 4300 5900 59  0000 L TNN
F 1 "ATMEGA328P-AU" H 4300 3200 59  0000 L BNN
F 2 "satshakit_cnc:TQFP32-08" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:VCC #P+07
U 1 1 FAFE20BD
P 2250 7200
F 0 "#P+07" H 2250 7200 50  0001 C CNN
F 1 "VCC" V 2150 7100 59  0000 L BNN
F 2 "" H 2250 7200 50  0001 C CNN
F 3 "" H 2250 7200 50  0001 C CNN
	1    2250 7200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CSM-7X-DU CRYSTAL1
U 1 1 99621192
P 3300 4200
F 0 "CRYSTAL1" H 3400 4240 59  0000 L BNN
F 1 "16Mhz" H 3400 4100 59  0000 L BNN
F 2 "satshakit_cnc:CSM-7X-DU" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C1
U 1 1 7EE2FFE3
P 2300 3800
F 0 "C1" H 2264 3909 69  0000 L BNN
F 1 "22pF" H 2225 3492 69  0000 L BNN
F 2 "Fab:C_1206" H 2300 3800 50  0001 C CNN
F 3 "" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C2
U 1 1 1A664D53
P 2300 4600
F 0 "C2" H 2264 4709 69  0000 L BNN
F 1 "22pF" H 2225 4292 69  0000 L BNN
F 2 "Fab:C_1206" H 2300 4600 50  0001 C CNN
F 3 "" H 2300 4600 50  0001 C CNN
	1    2300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:GND #GND01
U 1 1 8DA2BB24
P 2250 6300
F 0 "#GND01" H 2250 6300 50  0001 C CNN
F 1 "GND" H 2150 6200 59  0000 L BNN
F 2 "" H 2250 6300 50  0001 C CNN
F 3 "" H 2250 6300 50  0001 C CNN
	1    2250 6300
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R1
U 1 1 5FC72806
P 3000 3500
F 0 "R1" H 2850 3559 59  0000 L BNN
F 1 "10k" H 2850 3370 59  0000 L BNN
F 2 "Fab:R_1206" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:LED1206 LED_13
U 1 1 D663D747
P 8000 5800
F 0 "LED_13" V 8140 5720 59  0000 L BNN
F 1 "YELLOW" V 8225 5720 59  0000 L BNN
F 2 "Fab:LED_1206" H 8000 5800 50  0001 C CNN
F 3 "" H 8000 5800 50  0001 C CNN
	1    8000 5800
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R3
U 1 1 4F7F9F8C
P 8400 5800
F 0 "R3" H 8250 5859 59  0000 L BNN
F 1 "499" H 8250 5670 59  0000 L BNN
F 2 "Fab:R_1206" H 8400 5800 50  0001 C CNN
F 3 "" H 8400 5800 50  0001 C CNN
	1    8400 5800
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C3
U 1 1 C3EA450F
P 3650 6700
F 0 "C3" H 3710 6815 59  0000 L BNN
F 1 "10uF" H 3710 6615 59  0000 L BNN
F 2 "Fab:C_1206" H 3650 6700 50  0001 C CNN
F 3 "" H 3650 6700 50  0001 C CNN
	1    3650 6700
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C4
U 1 1 674C1B16
P 3150 6700
F 0 "C4" H 3210 6815 59  0000 L BNN
F 1 "1uF" H 3210 6615 59  0000 L BNN
F 2 "Fab:C_1206" H 3150 6700 50  0001 C CNN
F 3 "" H 3150 6700 50  0001 C CNN
	1    3150 6700
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C5
U 1 1 8E04CE4C
P 2650 6700
F 0 "C5" H 2710 6815 59  0000 L BNN
F 1 "100nF" H 2710 6615 59  0000 L BNN
F 2 "Fab:C_1206" H 2650 6700 50  0001 C CNN
F 3 "" H 2650 6700 50  0001 C CNN
	1    2650 6700
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C6
U 1 1 21A596BE
P 3700 2900
F 0 "C6" H 3760 3015 59  0000 L BNN
F 1 "100nF" H 3760 2815 59  0000 L BNN
F 2 "Fab:C_1206" H 3700 2900 50  0001 C CNN
F 3 "" H 3700 2900 50  0001 C CNN
	1    3700 2900
	0    -1   -1   0   
$EndComp
Text Notes 8550 4200 0    59   ~ 0
ENA (PWM)\nIN1\nIN2\nIN3\nIN4\nENB (PWM)
Text GLabel 8250 4100 0    50   Input ~ 0
ENA
Text GLabel 8250 3700 0    50   Input ~ 0
IN1
Text GLabel 8250 3800 0    50   Input ~ 0
IN2
Text GLabel 8250 3900 0    50   Input ~ 0
IN3
Text GLabel 8250 4000 0    50   Input ~ 0
IN4
Text GLabel 8250 4200 0    50   Input ~ 0
ENB
Text GLabel 6500 5400 2    50   Input ~ 0
ENA
Text GLabel 6500 5500 2    50   Input ~ 0
ENB
Text GLabel 8250 4300 0    50   Input ~ 0
GND
Text GLabel 6500 4900 2    50   Input ~ 0
IN1
Text GLabel 6500 5000 2    50   Input ~ 0
IN2
Text GLabel 6500 5100 2    50   Input ~ 0
IN3
Text GLabel 6500 5300 2    50   Input ~ 0
IN4
Text GLabel 8250 4400 0    50   Input ~ 0
GND
Text Notes 7500 3050 0    50   ~ 0
Signal (PWM)
Text GLabel 7250 3000 0    50   Input ~ 0
ServoSignal
Text GLabel 6500 4700 2    50   Input ~ 0
ServoSignal
Text GLabel 7250 3100 0    50   Input ~ 0
VCC
Text GLabel 7250 3200 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x04_Male BluetoothMod1
U 1 1 61C0AC86
P 8550 3100
F 0 "BluetoothMod1" H 9000 2650 50  0000 R CNN
F 1 "Conn_01x04_Male" H 9050 2750 50  0000 R CNN
F 2 "eagle_pin:04P" H 8550 3100 50  0001 C CNN
F 3 "~" H 8550 3100 50  0001 C CNN
	1    8550 3100
	-1   0    0    1   
$EndComp
Text GLabel 8350 2900 0    50   Input ~ 0
GND
Text GLabel 8350 3000 0    50   Input ~ 0
VCC
Text GLabel 8350 3100 0    50   Input ~ 0
TXD
Text GLabel 8350 3200 0    50   Input ~ 0
RXD
Text GLabel 6500 4400 2    50   Input ~ 0
RXD
Text GLabel 6500 4500 2    50   Input ~ 0
TXD
$Comp
L Connector:Conn_01x03_Male ServoMotor1
U 1 1 61C19960
P 7450 3100
F 0 "ServoMotor1" H 7600 2850 50  0000 R CNN
F 1 "ServoMotor" H 7800 2750 50  0000 R CNN
F 2 "eagle_pin:03P" H 7450 3100 50  0001 C CNN
F 3 "~" H 7450 3100 50  0001 C CNN
	1    7450 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 2900 4600 2900
Wire Wire Line
	3200 2900 3200 2450
Wire Wire Line
	3200 2450 4600 2450
Connection ~ 3200 2900
Text GLabel 4600 2450 2    50   Input ~ 0
Connector
Text GLabel 4600 2900 2    50   Input ~ 0
Interface
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 61C29DBC
P 5950 2700
F 0 "J2" H 5922 2582 50  0000 R CNN
F 1 "Conn_01x02_Male" H 5922 2673 50  0000 R CNN
F 2 "eagle_pin:02P" H 5950 2700 50  0001 C CNN
F 3 "~" H 5950 2700 50  0001 C CNN
	1    5950 2700
	-1   0    0    1   
$EndComp
Text GLabel 5750 2600 0    50   Input ~ 0
Connector
Text GLabel 5750 2700 0    50   Input ~ 0
Interface
$Comp
L Connector:Conn_01x08_Male L298N1
U 1 1 61C31A72
P 8450 4100
F 0 "L298N1" H 8700 3400 50  0000 R CNN
F 1 "Conn_01x08_Male" H 8950 3550 50  0000 R CNN
F 2 "eagle_pin:08P" H 8450 4100 50  0001 C CNN
F 3 "~" H 8450 4100 50  0001 C CNN
	1    8450 4100
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x08_Male J3
U 1 1 61BBC925
P 7350 3900
F 0 "J3" H 7322 3782 50  0000 R CNN
F 1 "Conn_01x08_Male" H 7322 3873 50  0000 R CNN
F 2 "eagle_pin:08P" H 7350 3900 50  0001 C CNN
F 3 "~" H 7350 3900 50  0001 C CNN
	1    7350 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 4800 6800 4800
Wire Wire Line
	6800 4800 6800 5000
Wire Wire Line
	6800 5000 7450 5000
Wire Wire Line
	6500 5600 6800 5600
Wire Wire Line
	6800 5600 6800 5100
Wire Wire Line
	6800 5100 7450 5100
Wire Wire Line
	6500 5700 6850 5700
Wire Wire Line
	6850 5700 6850 5200
Wire Wire Line
	6850 5200 7450 5200
Wire Wire Line
	6500 4600 7050 4600
Wire Wire Line
	7050 4600 7050 4900
Wire Wire Line
	7050 4900 7450 4900
$Comp
L Connector:Conn_01x06_Male VCC&GND_extras1
U 1 1 61BD759C
P 3050 5500
F 0 "VCC&GND_extras1" H 3158 5881 50  0000 C CNN
F 1 "Conn_01x06_Male" H 3158 5790 50  0000 C CNN
F 2 "eagle_pin:06P" H 3050 5500 50  0001 C CNN
F 3 "~" H 3050 5500 50  0001 C CNN
	1    3050 5500
	1    0    0    -1  
$EndComp
Text Label 3250 5300 0    70   ~ 0
VCC
Text Label 3250 5400 0    70   ~ 0
VCC
Text Label 3250 5500 0    70   ~ 0
VCC
Text Label 3250 5600 0    70   ~ 0
GND
Text Label 3250 5700 0    70   ~ 0
GND
Text Label 3250 5800 0    70   ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 61C018F8
P 7650 5000
F 0 "J1" H 7758 5181 50  0000 C CNN
F 1 "Conn_01x02_Male" H 7300 4850 50  0000 C CNN
F 2 "eagle_pin:02P" H 7650 5000 50  0001 C CNN
F 3 "~" H 7650 5000 50  0001 C CNN
	1    7650 5000
	-1   0    0    1   
$EndComp
Text GLabel 6500 3500 2    50   Input ~ 0
PC0
Text GLabel 6500 3600 2    50   Input ~ 0
PC1
Text GLabel 6500 3700 2    50   Input ~ 0
PC2
Text GLabel 6500 3800 2    50   Input ~ 0
PC3
Text GLabel 6500 3900 2    50   Input ~ 0
PC4
Text GLabel 6500 4000 2    50   Input ~ 0
PC5
Text GLabel 6500 4100 2    50   Input ~ 0
ADC6
Text GLabel 6500 4200 2    50   Input ~ 0
ADC7
Text GLabel 7150 3500 0    50   Input ~ 0
ADC6
Text GLabel 7150 3600 0    50   Input ~ 0
ADC7
Text GLabel 7150 3700 0    50   Input ~ 0
PC0
Text GLabel 7150 3800 0    50   Input ~ 0
PC1
Text GLabel 7150 3900 0    50   Input ~ 0
PC2
Text GLabel 7150 4000 0    50   Input ~ 0
PC3
Text GLabel 7150 4100 0    50   Input ~ 0
PC4
Text GLabel 7150 4200 0    50   Input ~ 0
PC5
$Comp
L Regulator_Linear:LM7805_TO220 VoltageRegulator(5V)1
U 1 1 61BC520B
P 5450 6450
F 0 "VoltageRegulator(5V)1" H 5450 6692 50  0000 C CNN
F 1 "LM7805_TO220" H 5450 6601 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5450 6675 50  0001 C CIN
F 3 "https://www.onsemi.cn/PowerSolutions/document/MC7800-D.PDF" H 5450 6400 50  0001 C CNN
	1    5450 6450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 61BDC21C
P 4250 6550
F 0 "J5" H 4358 6831 50  0000 C CNN
F 1 "Battery VCC (11.1V)" H 4358 6740 50  0000 C CNN
F 2 "eagle_pin:04P" H 4250 6550 50  0001 C CNN
F 3 "~" H 4250 6550 50  0001 C CNN
	1    4250 6550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J6
U 1 1 61BDE881
P 4250 7150
F 0 "J6" H 4358 7431 50  0000 C CNN
F 1 "Battery GND" H 4358 7340 50  0000 C CNN
F 2 "eagle_pin:04P" H 4250 7150 50  0001 C CNN
F 3 "~" H 4250 7150 50  0001 C CNN
	1    4250 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 7050 5450 6750
Wire Wire Line
	4450 6450 4900 6450
Wire Wire Line
	4450 6750 4450 6650
Connection ~ 4450 6450
Connection ~ 4450 6550
Wire Wire Line
	4450 6550 4450 6450
Connection ~ 4450 6650
Wire Wire Line
	4450 6650 4450 6550
Wire Wire Line
	4450 7050 4900 7050
Wire Wire Line
	4450 7350 4450 7250
Connection ~ 4450 7050
Connection ~ 4450 7150
Wire Wire Line
	4450 7150 4450 7050
Connection ~ 4450 7250
Wire Wire Line
	4450 7250 4450 7150
Wire Wire Line
	6400 6450 6400 6050
Text GLabel 6400 6050 1    50   Input ~ 0
VCC
$Comp
L Connector:Conn_01x04_Male J7
U 1 1 61C1DCD1
P 6600 6650
F 0 "J7" H 6572 6532 50  0000 R CNN
F 1 "Conn_01x04_Male" H 6572 6623 50  0000 R CNN
F 2 "eagle_pin:04P" H 6600 6650 50  0001 C CNN
F 3 "~" H 6600 6650 50  0001 C CNN
	1    6600 6650
	-1   0    0    1   
$EndComp
Connection ~ 6400 6450
$Comp
L Connector:Conn_01x04_Male J8
U 1 1 61C1F2EC
P 6600 7250
F 0 "J8" H 6572 7132 50  0000 R CNN
F 1 "Conn_01x04_Male" H 6572 7223 50  0000 R CNN
F 2 "eagle_pin:04P" H 6600 7250 50  0001 C CNN
F 3 "~" H 6600 7250 50  0001 C CNN
	1    6600 7250
	-1   0    0    1   
$EndComp
$Comp
L Fab:C 10uF1
U 1 1 61C2274F
P 4900 6750
F 0 "10uF1" H 5015 6796 50  0000 L CNN
F 1 "C" H 5015 6705 50  0000 L CNN
F 2 "Fab:C_1206" H 4938 6600 50  0001 C CNN
F 3 "" H 4900 6750 50  0001 C CNN
	1    4900 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6450 6000 6450
Wire Wire Line
	5450 7050 6000 7050
Connection ~ 5450 7050
$Comp
L Fab:C 10uF2
U 1 1 61C27B66
P 6000 6750
F 0 "10uF2" H 6115 6796 50  0000 L CNN
F 1 "C" H 6115 6705 50  0000 L CNN
F 2 "Fab:C_1206" H 6038 6600 50  0001 C CNN
F 3 "" H 6000 6750 50  0001 C CNN
	1    6000 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 6600 6000 6450
Connection ~ 6000 6450
Wire Wire Line
	6000 6450 6400 6450
Wire Wire Line
	6000 6900 6000 7050
Connection ~ 6000 7050
Wire Wire Line
	6000 7050 6400 7050
Wire Wire Line
	4900 6600 4900 6450
Connection ~ 4900 6450
Wire Wire Line
	4900 6450 5150 6450
Wire Wire Line
	4900 6900 4900 7050
Connection ~ 4900 7050
Wire Wire Line
	4900 7050 5450 7050
Wire Wire Line
	6400 6450 6400 6550
Connection ~ 6400 6550
Wire Wire Line
	6400 6550 6400 6650
Connection ~ 6400 6650
Wire Wire Line
	6400 6650 6400 6750
Wire Wire Line
	6400 7050 6400 7150
Connection ~ 6400 7050
Connection ~ 6400 7150
Wire Wire Line
	6400 7150 6400 7250
Connection ~ 6400 7250
Wire Wire Line
	6400 7250 6400 7350
Connection ~ 6400 7350
Wire Wire Line
	6400 7350 6400 7450
Text GLabel 6400 7450 3    50   Input ~ 0
GND
Text Label 3900 5000 2    70   ~ 0
VCC
Wire Wire Line
	3900 5000 4100 5000
Wire Wire Line
	6500 5800 6900 5800
$Comp
L Connector:Conn_01x03_Male Programmingpins1
U 1 1 61CAB348
P 7650 5200
F 0 "Programmingpins1" H 7622 5132 50  0000 R CNN
F 1 "Conn_01x03_Male" H 7622 5223 50  0000 R CNN
F 2 "eagle_pin:03P" H 7650 5200 50  0001 C CNN
F 3 "~" H 7650 5200 50  0001 C CNN
	1    7650 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	7450 5300 6900 5300
Wire Wire Line
	6900 5300 6900 5800
Connection ~ 6900 5800
Wire Wire Line
	6900 5800 7800 5800
$Comp
L Fab:C 0OhmResistor1
U 1 1 61CD0245
P 7900 6450
F 0 "0OhmResistor1" H 8015 6496 50  0000 L CNN
F 1 "C" H 8015 6405 50  0000 L CNN
F 2 "Fab:C_1206" H 7938 6300 50  0001 C CNN
F 3 "" H 7900 6450 50  0001 C CNN
	1    7900 6450
	1    0    0    -1  
$EndComp
$Comp
L Fab:C 0OhmResistor2
U 1 1 61CD1953
P 8200 6450
F 0 "0OhmResistor2" H 8315 6496 50  0000 L CNN
F 1 "C" H 8315 6405 50  0000 L CNN
F 2 "Fab:C_1206" H 8238 6300 50  0001 C CNN
F 3 "" H 8200 6450 50  0001 C CNN
	1    8200 6450
	1    0    0    -1  
$EndComp
Text GLabel 8200 6300 1    50   Input ~ 0
GND
Text GLabel 7900 6300 1    50   Input ~ 0
GND
Text GLabel 7900 6600 3    50   Input ~ 0
GND
Text GLabel 8200 6600 3    50   Input ~ 0
GND
$Comp
L Fab:C 0OhmResistor3
U 1 1 61CFE6AD
P 8600 6450
F 0 "0OhmResistor3" H 8715 6496 50  0000 L CNN
F 1 "C" H 8715 6405 50  0000 L CNN
F 2 "Fab:C_1206" H 8638 6300 50  0001 C CNN
F 3 "" H 8600 6450 50  0001 C CNN
	1    8600 6450
	1    0    0    -1  
$EndComp
Text GLabel 8600 6300 1    50   Input ~ 0
GND
Text GLabel 8600 6600 3    50   Input ~ 0
GND
$EndSCHEMATC
