#include <SoftwareSerial.h>

SoftwareSerial mySerial(4, 3); // RX, TX
int potPin= A4;  //Declare potPin to be analog pin A5
int LEDPin= 5;  // Declare LEDPin to be arduino pin 5
int readValue;  // Use this variable to read Potentiometer
int LEDbrightness; // Use this variable for writing to LED

void setup() {
  // put your setup code here, to run once:
  pinMode(potPin, INPUT);  //set potPin to be an input
  pinMode(LEDPin, OUTPUT); //set LEDPin to be an OUTPUT
    mySerial.begin(4800);
}

void loop() {
  // put your main code here, to run repeatedly:



 readValue = analogRead(potPin);  //Read the voltage on the Potentiometer
 LEDbrightness = map(readValue, 0, 1023, 0, 255);
 analogWrite(LEDPin, LEDbrightness);      //Write to the LED
 mySerial.print("LED value: ");  //for debugging print your values
 mySerial.println(LEDbrightness);
  mySerial.print("Potentiometer value: ");
 mySerial.println(analogRead(A4));
  delay(1000);
}
