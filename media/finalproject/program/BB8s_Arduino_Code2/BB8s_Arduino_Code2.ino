//#include <Servo.h> //Servo library
//Servo servoMain; 

char dataIn='S'; //Default Signal is "Stop"
char determinant;
char det;
int vel = 60;

void setup(){
Serial.begin(9600); //Start the serial communication
  
pinMode(13, OUTPUT); //Defines the PCB's LED pin as output
  
//Declaration for the pins used
pinMode(5, OUTPUT); //1 Motor Direction
pinMode(6, OUTPUT); //1 Motor Speed
pinMode(8, OUTPUT); //2 Motor Direction
pinMode(9, OUTPUT); //2 Motor Speed

/*
servoMain.attach(3);delay(100);
delay(1000); //servo control
servoMain.write(0);
delay(1000);
servoMain.write(90);
*/
}

void loop(){
    digitalWrite(13, HIGH);   // turn the LED on  
    
    det = checkDirection(); //writes return value of function into the variable
      
      while (det == 'F'){ // F, move forward (LOW = clockwise direction)
        digitalWrite(5, LOW);  
        analogWrite(6,vel);
        digitalWrite(8, LOW);  
        analogWrite(9,vel);
        det = checkDirection();}

      while (det == 'B'){ // B, move back (HIGH = counter-clockwise direction)
        digitalWrite(5, HIGH);  
        analogWrite(6,vel);
        digitalWrite(8, HIGH);  
        analogWrite(9,vel);
        det = checkDirection();}

      while (det == 'L'){ // L, move wheels left
        digitalWrite(5, HIGH);  
        analogWrite(6,vel);
        digitalWrite(8, LOW);  
        analogWrite(9,vel);
        det = checkDirection();}

      while (det == 'R'){ // R, move wheels right
        digitalWrite(5, LOW);  
        analogWrite(6,vel);
        digitalWrite(8, HIGH);  
        analogWrite(9,vel);
        det = checkDirection();}
        
      while (det == 'I'){ // I, turn right forward 
        digitalWrite(5, LOW);  
        analogWrite(6,vel);
        digitalWrite(8, LOW);  
        analogWrite(9,vel/2);
        det = checkDirection();}  
          
      while (det == 'J'){ // J, turn right back
        digitalWrite(5, HIGH);  
        analogWrite(6,vel);
        digitalWrite(8, HIGH);  
        analogWrite(9,vel/2);
        det = checkDirection();} 
    
      while (det == 'G'){ // G, turn left forward
        digitalWrite(5, LOW);  
        analogWrite(6,vel/2);
        digitalWrite(8, LOW);  
        analogWrite(9,vel);
        det = checkDirection();} 
     
      while (det == 'H'){ // H, turn left back
        digitalWrite(5, HIGH);  
        analogWrite(6,vel/2);
        digitalWrite(8, HIGH);  
        analogWrite(9,vel);
        det = checkDirection();} 
    
      while (det == 'S'){ // S, stop
        digitalWrite(5, LOW);  
        analogWrite(6,0);
        digitalWrite(8, LOW);  
        analogWrite(9,0);
        det = checkDirection();}
           
      //Head movement with servo
  //while (det == 'W'){servoMain.write(180);delay(100);det = checkDirection();}  //W and w = Front lights on/off
  //while (det == 'w'){servoMain.write(90);delay(100);det = checkDirection();} 
       
  // while (det == 'U'){servoMain.write(0);delay(100);det = checkDirection();}    //U and u = Back light on/off
  // while (det == 'u'){servoMain.write(90);delay(100);det = checkDirection();}    
}

int checkDirection() //function to check direction
    {if (Serial.available() > 0) {dataIn = Serial.read(); 
        if (dataIn == 'F'){determinant = 'F';} 
        else if (dataIn == 'B'){determinant = 'B';}
        else if (dataIn == 'L'){determinant = 'L';}
        else if (dataIn == 'R'){determinant = 'R';}
        else if (dataIn == 'I'){determinant = 'I';} 
        else if (dataIn == 'J'){determinant = 'J';}
        else if (dataIn == 'G'){determinant = 'G';}   
        else if (dataIn == 'H'){determinant = 'H';}
        else if (dataIn == 'S'){determinant = 'S';}
        
        else if (dataIn == '0'){vel = 40;}
        else if (dataIn == '1'){vel = 60;}
        else if (dataIn == '2'){vel = 75;}
        else if (dataIn == '3'){vel = 100;}        
        else if (dataIn == '4'){vel = 125;}
        else if (dataIn == '5'){vel = 150;}
        else if (dataIn == '6'){vel = 175;}
        else if (dataIn == '7'){vel = 200;}
        else if (dataIn == '8'){vel = 225;}
        else if (dataIn == 'q'){vel = 250;}
        
        else if (dataIn == 'U'){determinant = 'U';}else if (dataIn == 'u'){determinant = 'u';}
        else if (dataIn == 'W'){determinant = 'W';}else if (dataIn == 'w'){determinant = 'w';}               
        }return determinant;} //returns number that indicates direction
