#include <SoftwareSerial.h>

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 7;     // the number of the pushbutton pin
const int ledPin =  1;      // the number of the LED pin

// variables will change:
int amountPressed= 0;   //the amount the button was pressed
int lastButtonState;    // the previous state of button
int currentButtonState; // the current state of button

SoftwareSerial mySerial(4, 3); // RX, TX

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input with pullup:
  pinMode(buttonPin, INPUT_PULLUP);
  //the current state of the button
  currentButtonState = digitalRead(buttonPin);

    // Open serial communications and wait for port to open:
  mySerial.begin(4800);


}
  

void loop() {
  lastButtonState    = currentButtonState;      // save the last state
  currentButtonState = digitalRead(buttonPin); // read new state
  // check if the button has been toggled
  if (lastButtonState == LOW && currentButtonState == HIGH) { 
    amountPressed++; //increment the amount button has been pressed
    
    for(int i=0; i < amountPressed; i++){ //blink led for amount of times button has been pressed so far
    mySerial.print("Blink");  
    digitalWrite(ledPin, HIGH);   //turn LED on
    delay(300);                   //wait for a bit
    digitalWrite(ledPin, LOW);    //turn the LED off by making the voltage LOW
    delay(300);                   //wait for a bit  
    }
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);  
  }


}
